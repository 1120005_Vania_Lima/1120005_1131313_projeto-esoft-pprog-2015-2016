/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes;

/**
 *
 * @author vanialima
 */
public class FAE {

    private String nome;
    private int id;

    private static final String NOME_POR_OMISSAO = "sem nome";
    private static final int ID_POR_OMISSAO = 0;

    public FAE(String nome, int id) {
        this.nome = nome;
        this.id = id;
    }

    public FAE() {
        this.nome = NOME_POR_OMISSAO;
        this.id = ID_POR_OMISSAO;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "FAE com o " + " nome " + nome + "e  id : " + id;
    }
}

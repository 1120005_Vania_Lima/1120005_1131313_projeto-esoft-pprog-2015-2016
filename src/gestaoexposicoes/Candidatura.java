/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes;

/**
 *
 * @author vanialima
 */
public class Candidatura {
    
    private String nome;
    private String morada;
    private int telemovel;
    private float areaPretendida;
    private int quantidadeConvites;
    
    private static final String NOME_POR_OMISSAO = "sem nome";
    private static final String MORADA_POR_OMISSAO = "semo morada";
    private static final int TELEMOVEL_POR_OMISSAO = 0;
    private static final float AREAPRETENDIDA_POR_OMISSAO = 0.0f;
    private static final int QUANTIDADECONVITES_POR_OMISSAO = 0;

    public Candidatura(String nome, String morada, int telemovel, float areaPretendida, int quantidadeConvites) {
        this.nome = nome;
        this.morada = morada;
        this.telemovel = telemovel;
        this.areaPretendida = areaPretendida;
        this.quantidadeConvites = quantidadeConvites;
    }

    public Candidatura() {
        this.nome = NOME_POR_OMISSAO;
        this.morada = MORADA_POR_OMISSAO;
        this.telemovel = TELEMOVEL_POR_OMISSAO;
        this.areaPretendida = AREAPRETENDIDA_POR_OMISSAO;
        this.quantidadeConvites = QUANTIDADECONVITES_POR_OMISSAO;
    }
        

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMorada() {
        return morada;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    public int getTelemovel() {
        return telemovel;
    }

    public void setTelemovel(int telemovel) {
        this.telemovel = telemovel;
    }

    public float getAreaPretendida() {
        return areaPretendida;
    }

    public void setAreaPretendida(float areaPretendida) {
        this.areaPretendida = areaPretendida;
    }

    public int getQuantidadeConvites() {
        return quantidadeConvites;
    }

    public void setQuantidadeConvites(int quantidadeConvites) {
        this.quantidadeConvites = quantidadeConvites;
    }

    @Override
    public String toString() {
        return "Candidatura com o nome " + nome + ", morada " + morada + ", telemovel " + telemovel + ", areaPretendida " + areaPretendida + ", quantidadeConvites " + quantidadeConvites;
    }
    
}

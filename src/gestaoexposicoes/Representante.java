/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes;

/**
 *
 * @author vanialima
 */
public class Representante {
    
    private String nome;
    
    private static final String NOME_POR_OMISSAO = "sem nome";

    public Representante(String nome) {
        this.nome = nome;
    }

    public Representante() {
        this.nome = NOME_POR_OMISSAO;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "Representante com o nome " + nome;
    }
}

package gestaoexposicoes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author vanialima
 */
public class Exposicao {

    private String m_strTitulo;
    private String m_strDescritivo;
    private Date m_dtDtIni;
    private Date m_dtDtFim;
    private String m_strLocal;
    private final List<Organizador> m_listaOrganizadores;
//    private final List<FAE> m_listaFAR;

    public Exposicao() {
        m_listaOrganizadores = new ArrayList<Organizador>();
    }

    public boolean valida() {
        System.out.println("Exposição: valida: " + this.toString());
        return true;
    }

    @Override
    public String toString() {
        String str = "Exposição:\n";
        str += "\tTitulo: " + this.m_strTitulo + "\n";
        str += "\tTexto descritivo: " + this.m_strDescritivo + "\n";
        str += "\tData inicial: " + utils.Utils.convertDateToString(this.m_dtDtIni) + "\n";
        str += "\tData final: " + utils.Utils.convertDateToString(this.m_dtDtFim) + "\n";
        str += "\tLocal: " + this.m_strLocal + "\n";
        str += "\tOrganizadores:\n";
        for (Organizador o : m_listaOrganizadores) {
            str += "\t\t" + o.getUtilizador().getUser() + "\n";
        }

        return str;
    }

    public void setTitulo(String strTitulo) {
        this.m_strTitulo = strTitulo;
    }

    public void setDescritivo(String strDescritivo) {
        this.m_strDescritivo = strDescritivo;
    }

    public void setPeriodo(Date dtDtIni, Date dtDtFim) {
        this.m_dtDtIni = dtDtIni;
        this.m_dtDtFim = dtDtFim;
    }

    public void setLocal(String strLocal) {
        this.m_strLocal = strLocal;
    }

    private boolean validaOrganizador(Organizador o) {
        System.out.println("Exposição: validaOrganizador: " + o.toString());
        return true;
    }
    
//    private boolean validaFAE(FAE f){
//        System.out.println("FAE: validaFAE: " + f.toString());
//        return true;
//    }

    public void addOrganizador(Utilizador ut) {
        Organizador o = new Organizador();
        o.setUtilizador(ut);
        if (validaOrganizador(o)) {
            addOrganizador(o);
        }
    }

    private void addOrganizador(Organizador o) {
        m_listaOrganizadores.add(o);
    }
    
//    public void addMembroFae(FAE id){
//        FAE f = new FAE();
//        f.setOrganizador(o);
//        if (validaFAE(f)) {
//            addFAE(f);
//        }
//    }
//    
//    private void addFAE(FAE f){
//        m_listaFAE.add(f);
//    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes;

/**
 *
 * @author vanialima
 */
public class Decisao {

    private String decisao;
    private String justificacao;

    private static final String DECISAO_POR_OMISSAO = "sem decisao";
    private static final String JUSTIFICACAO_POR_OMISSAO = "sem justificacao";

    public Decisao(String decisao, String justificacao) {
        this.decisao = decisao;
        this.justificacao = justificacao;
    }

    public Decisao() {
        this.decisao = DECISAO_POR_OMISSAO;
        this.justificacao = JUSTIFICACAO_POR_OMISSAO;
    }

    public String getDecisao() {
        return decisao;
    }

    public void setDecisao(String decisao) {
        this.decisao = decisao;
    }

    public String getJustificacao() {
        return justificacao;
    }

    public void setJustificacao(String justificacao) {
        this.justificacao = justificacao;
    }

    @Override
    public String toString() {
        return "Decisao : " + decisao + "e justificacao : " + justificacao;
    }

}

package UIs;

import gestaoexposicoes.CentroExposicoes;
import java.io.IOException;
import utils.Utils;

/**
 *
 * @author vanialima
 */
public class MenuUI {

    private CentroExposicoes m_centro_exposicoes;
    private String opcao;

    public MenuUI(CentroExposicoes centro_exposicoes) {
        m_centro_exposicoes = centro_exposicoes;
    }

    public void run() throws IOException {
        do {
            System.out.println("1. Criar Exposição");
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            if (opcao.equals("1")) {
                CriarExposicaoUI uiCE = new CriarExposicaoUI(m_centro_exposicoes);
                uiCE.run();
            }

        } while (!opcao.equals("0"));
    }

}
